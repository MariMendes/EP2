/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import java.io.IOException;
import java.util.List;
import javax.xml.bind.JAXBException;

/**
 *
 * @author mariana
 */
public class Obter implements Runnable {
    
    private Camara listaDeputados;
    private List<Deputado> deputadosList;

    
    public Obter() {
        listaDeputados = new Camara();
        deputadosList = null;
        this.run();
        deputadosList = listaDeputados.getDeputados();   
    }
    
    @Override
    public void run() {
        try {
            listaDeputados.obterDados();
        }
        catch (JAXBException jaxbException) { 
        }
        catch (IOException camaraException) {
        }
        
     //@throws JAXBException se ocorrer algum problema na hora de interpretar o XML.
     //@throws IOException se algum erro ocorrer na hora de baixar os dados.
    }
}

    
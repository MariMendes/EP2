/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import java.io.IOException;
import java.util.List;
import javax.xml.bind.JAXBException;

/**
 *
 * @author mariana
 */
public class Ordena implements Runnable {
    
    private Camara listaDeputados;
    private List<Deputado> deputadosList;
    private List<Deputado> listaOrdenaPartido;
    private List<Deputado> listaOrdenaEstado;
    private List<Deputado> listaOrdenaCondicao;
    private List<Deputado> listaOrdenadaNome;
    private List<Deputado> listaResultados;

    public Ordena() {
        deputadosList = null;
        listaOrdenaPartido = null;
        listaOrdenaEstado = null;
        listaOrdenaCondicao = null;
        listaOrdenadaNome = null;
        listaResultados = null;
        this.run();
        deputadosList = listaDeputados.getDeputados();
        ordenaNome();
        buscaNome("rOn");

    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            listaDeputados.obterDados();
        }
        catch (JAXBException jaxbException) {
            //abrir painel de alerta com ok;
        }
        catch (IOException camaraException){
            //abrir painel;
        }
        
    }
    public void imprime(){
        while(this.deputadosList == null);
        for(int i = 0; i < deputadosList.size();i++){
            System.out.println(deputadosList.get(i).getNome());
            System.out.println(deputadosList.get(i).getSexo());
            System.out.println(deputadosList.get(i).getFone());
            System.out.println(deputadosList.get(i).getUrlFoto());
            System.out.println("////////////////////////////");
        }
    }
    
    public void ordenaNome(){ 
        Deputado auxiliar;
        listaOrdenadaNome = deputadosList;
        listaOrdenadaNome.toArray();
        int i, j;
        for( i = 1; i < listaOrdenadaNome.size(); i++){
            auxiliar = listaOrdenadaNome.get(i);
            for( j = i - 1; j >= 0; j--){
                if(auxiliar.getNome().compareTo(listaOrdenadaNome.get(j).getNome())< 0){
                    this.listaOrdenadaNome.set(j + 1, listaOrdenadaNome.get(j));
                }else{
                    break;
                }
            }
            this.listaOrdenadaNome.set(j+1, auxiliar);
        }
    }
    public void imprimeOrdenado(){
        while(this.listaOrdenadaNome == null);
        for(int i = 0; i < listaOrdenadaNome.size();i++){
            System.out.println(listaOrdenadaNome.get(i).getNome());
            System.out.println(listaOrdenadaNome.get(i).getSexo());
            System.out.println(listaOrdenadaNome.get(i).getFone());
            System.out.println(listaOrdenadaNome.get(i).getUrlFoto());
            System.out.println("////////////////////////////");
        }
    }
    public void buscaNome(String nome){
        listaResultados = null;
        listaResultados = deputadosList;
        //listaResultados.clear();
        int i = 0, j = 0;
        while(deputadosList==null);
        for(i = 0; i < deputadosList.size();i++){
            if(deputadosList.get(i).getNome().startsWith(toUpperCase(nome)) == true){
                listaResultados.set(j, deputadosList.get(i));
                j++;
            }
        }
        System.out.println(listaResultados.size());
        for(j = j; j != listaResultados.size(); ){
            //System.out.println(listaResultados.get(j).getNome());
            //System.out.println(listaResultados.get(j).getFone());
            listaResultados.remove(j);
            //System.out.println("REMOVEU ELEMENTO" +j);
        }
        for(int k=0; k < listaResultados.size(); k++){
            System.out.println(listaResultados.get(k).getNome());
            System.out.println(listaResultados.get(k).getFone());
        }
    }

    private String toUpperCase(String nome) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}